/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recursive_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: antgalan <antgalan@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/01 21:48:28 by antgalan          #+#    #+#             */
/*   Updated: 2022/11/04 12:40:00 by antgalan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_recursive_power(int nb, int power)
{
	if (0 < power)
		return (nb * ft_recursive_power(nb, (power - 1)));
	else if (power == 0)
		return (1);
	else
		return (0);
	return (nb);
}
