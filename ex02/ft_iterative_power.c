/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: antgalan <antgalan@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/31 14:37:08 by antgalan          #+#    #+#             */
/*   Updated: 2022/10/31 14:37:28 by antgalan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_iterative_power(int nb, int power)
{
	int	i;
	int	pw;

	pw = 1;
	if (0 <= power)
	{
		i = 0;
		while (i < power)
		{
			pw *= nb;
			i++;
		}
	}
	else
		return (0);
	return (pw);
}
