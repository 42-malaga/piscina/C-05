/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ten_queens_puzzle.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: antgalan <antgalan@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/02 13:31:10 by antgalan          #+#    #+#             */
/*   Updated: 2022/11/02 15:19:32 by antgalan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

/**
 * @brief	Prints a solution.
 *
 * @param sol	The solution to print.
 */
void	print_solution(const int *sol)
{
	int	num;
	int	i;

	i = 0;
	while (i < 10)
	{
		num = '0' + sol[i];
		write(1, &num, 1);
		i++;
	}
	write(1, "\n", 1);
}

/**
 * @brief	Checks if a queen can be placed in a given position.
 *
 * @param val	Queen's value to check.
 * @param pos	Position to check with the value.
 * @param brd	State of the board.
 *
 * @return	'1' if valid; '0' otherwise.
 */
int	is_posible(int val, int pos, const int *brd)
{
	int	p;
	int	i;

	i = 0;
	while (i < pos)
	{
		p = pos - i;
		if (val == brd[i])
			return (0);
		if (val == brd[i] + p && brd[i] + p < 10)
			return (0);
		if (val == brd[i] - p && -1 < brd[i] - p)
			return (0);
		i++;
	}
	return (1);
}

/**
 * @brief	Calculates all the solutions for the problem.
 *
 * @param pos	Queen's position in the actual column.
 * @param brd	State of the board.
 * @param cnt	Number of solutions counter.
 *
 * @return	Number of solutions found.
 */
int	calculate_solutions(int pos, int *brd, int cnt)
{
	int	i;

	if (pos < 10)
	{
		i = 0;
		while (i < 10)
		{
			if (is_posible(i, pos, brd))
			{
				brd[pos] = i;
				if (pos == 9)
				{
					print_solution(brd);
					return (cnt + 1);
				}
				cnt = calculate_solutions(pos + 1, brd, cnt);
			}
			i++;
		}
	}
	return (cnt);
}

int	ft_ten_queens_puzzle(void)
{
	int	state[10];

	return (calculate_solutions(0, state, 0));
}
